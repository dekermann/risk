package Game_model.resource.image_resources;

import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Tile {
	private Image img;
	public int x,y;
	public int w,h;
	
	public Tile(String filename) {
		if (!filename.toLowerCase().contains(".png")) {
			try {
				throw new Exception("Kolla om filnamnet �r r�tt ex: exempel.png");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		try {
			img = ImageIO.read(new File(filename));
			
			setTilePosition(0,0);
			setTileSize(img.getWidth(null), img.getHeight(null));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void setTileSize(int w, int h) {
		this.w = w;
		this.h = h;
	}
	
	public void setTilePosition(int x, int y) {
		this.x = x;
		this.y = y;
	}
}
