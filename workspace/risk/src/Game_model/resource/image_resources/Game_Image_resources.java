package Game_model.resource.image_resources;

import javax.swing.ImageIcon;

public class Game_Image_resources {

	private ImageIcon[] menu_button;
	private ImageIcon[] start_button;
	
	public static final int NORMAL = 0;
	public static final int PRESSED = 1;
	public static final int HOOVER = 2;
	public static final int PRESSED_HOOVER = 3;
	
	
	public Game_Image_resources() {
//		loadMenuButton();
//		loadStartButton();
	}
	
	@SuppressWarnings("unused")
	private void loadMenuButton() {
		menu_button[NORMAL] = new ImageIcon("MenuButton_Normal.JPG");
		menu_button[PRESSED] = new ImageIcon("MenuButton_Pressed.JPG");
		menu_button[HOOVER] = new ImageIcon("MenuButton_Hover.JPG");
		menu_button[PRESSED_HOOVER] = new ImageIcon("MenuButton_Pressed_Hover.JPG");
	}
	
	@SuppressWarnings("unused")
	private void loadStartButton() {
		start_button[NORMAL] = new ImageIcon("StartButton_Normal.JPG");
		start_button[PRESSED] = new ImageIcon("StartButton_Pressed.JPG");
		start_button[HOOVER] = new ImageIcon("StartButton_Hover.JPG");
		start_button[PRESSED_HOOVER] = new ImageIcon("StartButton_Pressed_Hover.JPG");
	}

	public ImageIcon[] getMenu_button() {
		return menu_button;
	}

	public ImageIcon[] getStart_button() {
		return start_button;
	}
}
