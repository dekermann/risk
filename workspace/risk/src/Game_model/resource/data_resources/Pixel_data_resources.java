package Game_model.resource.data_resources;

import java.awt.Point;
import java.util.ArrayList;
import java.util.HashMap;

import Game_model.resource.data_resources.helpers.GetBoundary;
import Game_model.resource.data_resources.helpers.PixelDetector;
import Game_model.resource.data_resources.helpers.World_controller;
import Game_model.resource.data_resources.helpers.World_manager;

public class Pixel_data_resources {

	World_controller wc;
	
	public Pixel_data_resources() {
		loadwc();
	}
	
	public World_controller getWc() {
		return this.wc;
	}
	
	private void loadwc() {
		PixelDetector PD = new PixelDetector("World.PNG");
		GetBoundary GB = new GetBoundary();
		HashMap<String, ArrayList<Point>> pixel_Data;
		HashMap<String, ArrayList<Point>> boundary_Data;
		pixel_Data = PD.getMap();
		boundary_Data = GB.getBoundaryMap(pixel_Data);
		
		setWc(new World_controller(new World_manager(boundary_Data, pixel_Data)));
	}
	
	private void setWc(World_controller wc) {
		this.wc = wc;
	}
}
