package Game_model.resource.data_resources.helpers;

import java.awt.Point;
import java.util.ArrayList;



public class World_controller {
	//Variable for world_manager and static NO_COUNTRY if clicked
	World_manager wm;
	public static final String NO_COUNTRY = "-1";
	
	public World_controller(World_manager wm) {
		setWorldManager(wm);
	}
	
	public World_manager getWm(){
		return wm;
	}
	
	//Returning the country id if point p matches the data
	// else returning "-1" aka no_country
	@SuppressWarnings("static-access")
	public String checkForCountry(Point p) {
		ArrayList<String> countries = getCountry_fromBounds(p);
		
		if (countries.size() == 1) {
			return countries.get(0);
		} else if (countries.size() > 1) {
			return getCountry_fromPixels(countries, p);
		}
		return this.NO_COUNTRY;
	}
	
	private ArrayList<String> getCountry_fromBounds(Point p) {
		ArrayList<String> inBounds = new ArrayList<String>();
		for (String s : wm.getCountryIds()) {
			if (checkBounds(wm.getBoundary(s), p)) {
				inBounds.add(s);
			}
		}
		return inBounds;
	}
	
	@SuppressWarnings("static-access")
	private String getCountry_fromPixels(ArrayList<String> candidates, Point p) {
		for (String s : candidates) {
			if (checkPixels(s, p)) {
				return s;
			}
		}
		return this.NO_COUNTRY;
	}
	
	public void setWorldManager(World_manager wm) {
		this.wm = wm;
	}
	
	private boolean checkPixels(String id, Point p) {
		for (Point temp : wm.getPixels(id)) {
			if (temp.equals(p)) {
				return true;
			}
		}
		return false;
	}
	
	@SuppressWarnings("static-access")
	private boolean checkBounds(ArrayList<Point> bounds, Point p) {
		int x = p.x;
		int y = p.y;
		//Getting top left,right and bottom left,right bounds
		Point tL = bounds.get(wm.TOP_LEFT);
		Point tR = bounds.get(wm.TOP_RIGHT);
		Point bL = bounds.get(wm.BOTTOM_LEFT);
		Point bR = bounds.get(wm.BOTTOM_RIGHT);
		
		//Bound check
		if (tL.x <= x && tL.y <= y) {
			if (tR.x >= x && tL.y <= y) {
				if (bL.x <= x && bL.y >= y) {
					if (bR.x >= x && bR.y >= y) {
						return true;
					}
				}
			}
		}
		
		//If not in bounds return false
		return false;
	}
}
