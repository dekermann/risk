package Game_model.resource.data_resources.helpers;

import java.awt.Color;
import java.util.HashMap;

import Game_model.resource.Game_resources;



public class Country_Manager {
	
	private final Color RED = new Color(200,50,50);
	private final Color BLUE = new Color(50,50,200);
	private final Color GREEN = new Color(50,200,50);
	private final Color YELLOW = new Color(150,150,50);
	private final Color PINK = new Color(255,20,150);
	private final Color TEAL = new Color(50,150,150);
	private final Color SELECTED = new Color(255,255,255,75);
	
	private Country current = null;
	private Country pre_current = null;
	private Country Alaska = new Country("Alaska");
	private Country North_Canada = new Country("North Canada");
	private Country West_Canada = new Country("West Canada");
	private Country Central_Canada = new Country("Central Canada");
	private Country East_Canada = new Country("East Canada");
	private Country West_USA = new Country("West USA");
	private Country East_USA = new Country("East USA");
	private Country Mexico = new Country("Mexico");
	private Country Greenland = new Country("Greenland");
	private Country North_South_America = new Country("North South America");
	private Country West_South_America = new Country("West South America");
	private Country East_South_America = new Country("East South America");
	private Country South_South_America = new Country("South South America");
	private Country Iceland = new Country("Iceland");
	private Country Scandinavia = new Country("Scandinavia");
	private Country Great_Britain = new Country("Great Britain");
	private Country Central_Europe = new Country("Central Europe");
	private Country West_Europe = new Country("West Europe");
	private Country South_Europe = new Country("South Europe");
	private Country East_Europe = new Country("East Europe");
	private Country Northwest_Africa = new Country("Northwest Africa");
	private Country North_Africa = new Country("North Africa");
	private Country Central_Africa = new Country("Central Africa");
	private Country East_Africa = new Country("East Africa");
	private Country South_Africa = new Country("South Africa");
	private Country Madagascar = new Country("Madagascar");
	private Country West_Australia = new Country("West Australia");
	private Country East_Australia = new Country("East Australia");
	private Country Indonesia = new Country("Indonesia");
	private Country New_Guinea = new Country("New Guinea");
	private Country West_Russia = new Country("West Russia");
	private Country CentralWest_Russia = new Country("CentralWest Russia");
	private Country CentralEast_Russia = new Country("CentralEast Russia");
	private Country East_Russia = new Country("East Russia");
	private Country Middle_East = new Country("Middle East");
	private Country Afghanistan = new Country("Afghanistan");
	private Country India = new Country("India");
	private Country South_Russia = new Country("South Russia");
	private Country Mongolia = new Country("Mongolia");
	private Country China = new Country("China");
	private Country Korea = new Country("Korea");
	private Country Japan = new Country("Japan");
	private HashMap<String, Country> CountryMatrix;
	private Game_resources gr;
	
	public Country_Manager(Game_resources gr){
		this.gr = gr;
		// LITE TEST KOD F�R ATT S�TTA DIT OWNERS AV L�NDER
		China.setOwner("RED");
		Scandinavia.setOwner("RED");
		Alaska.setOwner("RED");
		West_Canada.setOwner("RED");
		Madagascar.setOwner("RED");
		South_Africa.setOwner("RED");
		West_Australia.setOwner("TEAL");
		East_Australia.setOwner("TEAL");
		// ---- H�R SLUTAR TEST KODEN ----
		Mexico.setOwner("TEAL");
		North_South_America.setOwner("TEAL");
		South_South_America.setOwner("TEAL");
		CountryMatrix = new HashMap<String, Country>();
		CountryMatrix.put("Alaska", Alaska);
		CountryMatrix.put("North Canada", North_Canada);
		CountryMatrix.put("West Canada", West_Canada);
		CountryMatrix.put("Central Canada", Central_Canada);
		CountryMatrix.put("East Canada", East_Canada);
		CountryMatrix.put("West USA", West_USA);
		CountryMatrix.put("East USA", East_USA);
		CountryMatrix.put("Mexico", Mexico);
		CountryMatrix.put("Greenland", Greenland);
		CountryMatrix.put("North South America", North_South_America);
		CountryMatrix.put("West South America", West_South_America);
		CountryMatrix.put("East South America", East_South_America);
		CountryMatrix.put("South South America", South_South_America);
		CountryMatrix.put("Iceland", Iceland);
		CountryMatrix.put("Scandinavia", Scandinavia);
		CountryMatrix.put("Great Britain", Great_Britain);
		CountryMatrix.put("Central Europe", Central_Europe);
		CountryMatrix.put("West Europe", West_Europe);
		CountryMatrix.put("South Europe", South_Europe);
		CountryMatrix.put("East Europe", East_Europe);
		CountryMatrix.put("Northwest Africa", Northwest_Africa);
		CountryMatrix.put("North Africa", North_Africa);
		CountryMatrix.put("Central Africa", Central_Africa);
		CountryMatrix.put("East Africa", East_Africa);
		CountryMatrix.put("South Africa", South_Africa);
		CountryMatrix.put("Madagascar", Madagascar);
		CountryMatrix.put("West Australia", West_Australia);
		CountryMatrix.put("East Australia", East_Australia);
		CountryMatrix.put("Indonesia", Indonesia);
		CountryMatrix.put("New Guinea", New_Guinea);
		CountryMatrix.put("West Russia", West_Russia);
		CountryMatrix.put("CentralWest Russia", CentralWest_Russia);
		CountryMatrix.put("CentralEast Russia", CentralEast_Russia);
		CountryMatrix.put("East Russia", East_Russia);
		CountryMatrix.put("Middle East", Middle_East);
		CountryMatrix.put("Afghanistan", Afghanistan);
		CountryMatrix.put("India", India);
		CountryMatrix.put("South Russia", South_Russia);
		CountryMatrix.put("Mongolia", Mongolia);
		CountryMatrix.put("China", China);
		CountryMatrix.put("Korea", Korea);
		CountryMatrix.put("Japan", Japan);
		setUpConnections();
	}
	
	private void setUpConnections(){
		CountryMatrix.get("Alaska").setConnection(CountryMatrix.get("North Canada"), 1);
		CountryMatrix.get("Alaska").setConnection(CountryMatrix.get("West Canada"), 1);
		CountryMatrix.get("Alaska").setConnection(CountryMatrix.get("East Russia"), 1);
		CountryMatrix.get("North Canada").setConnection(CountryMatrix.get("West Canada"), 1);
		CountryMatrix.get("North Canada").setConnection(CountryMatrix.get("Central Canada"), 1);
		CountryMatrix.get("North Canada").setConnection(CountryMatrix.get("Greenland"), 1);
		CountryMatrix.get("West Canada").setConnection(CountryMatrix.get("Central Canada"), 1);
		CountryMatrix.get("West Canada").setConnection(CountryMatrix.get("West USA"), 1);
		CountryMatrix.get("Central Canada").setConnection(CountryMatrix.get("East Canada"), 1);
		CountryMatrix.get("Central Canada").setConnection(CountryMatrix.get("West USA"), 1);
		CountryMatrix.get("Central Canada").setConnection(CountryMatrix.get("East USA"), 1);
		CountryMatrix.get("East Canada").setConnection(CountryMatrix.get("Greenland"), 1);
		CountryMatrix.get("East Canada").setConnection(CountryMatrix.get("East USA"), 1);
		CountryMatrix.get("West USA").setConnection(CountryMatrix.get("East USA"), 1);
		CountryMatrix.get("West USA").setConnection(CountryMatrix.get("Mexico"), 1);
		CountryMatrix.get("East USA").setConnection(CountryMatrix.get("Mexico"), 1);
		CountryMatrix.get("Mexico").setConnection(CountryMatrix.get("North South America"), 1);
		CountryMatrix.get("Greenland").setConnection(CountryMatrix.get("Iceland"), 1);
		CountryMatrix.get("North South America").setConnection(CountryMatrix.get("West South America"), 1);
		CountryMatrix.get("North South America").setConnection(CountryMatrix.get("East South America"), 1);
		CountryMatrix.get("West South America").setConnection(CountryMatrix.get("East South America"), 1);
		CountryMatrix.get("West South America").setConnection(CountryMatrix.get("South South America"), 1);
		CountryMatrix.get("East South America").setConnection(CountryMatrix.get("Northwest Africa"), 1);
		CountryMatrix.get("East South America").setConnection(CountryMatrix.get("South South America"), 1);
		CountryMatrix.get("Iceland").setConnection(CountryMatrix.get("Great Britain"), 1);
		CountryMatrix.get("Iceland").setConnection(CountryMatrix.get("Scandinavia"), 1);
		CountryMatrix.get("Scandinavia").setConnection(CountryMatrix.get("East Europe"), 1);
		CountryMatrix.get("Scandinavia").setConnection(CountryMatrix.get("Central Europe"), 1);
		CountryMatrix.get("Great Britain").setConnection(CountryMatrix.get("West Europe"), 1);
		CountryMatrix.get("Great Britain").setConnection(CountryMatrix.get("Central Europe"), 1);
		CountryMatrix.get("Central Europe").setConnection(CountryMatrix.get("East Europe"), 1);
		CountryMatrix.get("Central Europe").setConnection(CountryMatrix.get("South Europe"), 1);
		CountryMatrix.get("Central Europe").setConnection(CountryMatrix.get("West Europe"), 1);
		CountryMatrix.get("West Europe").setConnection(CountryMatrix.get("South Europe"), 1);
		CountryMatrix.get("West Europe").setConnection(CountryMatrix.get("Northwest Africa"), 1);
		CountryMatrix.get("South Europe").setConnection(CountryMatrix.get("East Europe"), 1);
		CountryMatrix.get("South Europe").setConnection(CountryMatrix.get("Northwest Africa"), 1);
		CountryMatrix.get("South Europe").setConnection(CountryMatrix.get("Middle East"), 1);
		CountryMatrix.get("East Europe").setConnection(CountryMatrix.get("West Russia"), 1);
		CountryMatrix.get("East Europe").setConnection(CountryMatrix.get("Afghanistan"), 1);
		CountryMatrix.get("East Europe").setConnection(CountryMatrix.get("Middle East"), 1);
		CountryMatrix.get("Northwest Africa").setConnection(CountryMatrix.get("North Africa"), 1);
		CountryMatrix.get("Northwest Africa").setConnection(CountryMatrix.get("East Africa"), 1);
		CountryMatrix.get("Northwest Africa").setConnection(CountryMatrix.get("Central Africa"), 1);
		CountryMatrix.get("North Africa").setConnection(CountryMatrix.get("Middle East"), 1);
		CountryMatrix.get("North Africa").setConnection(CountryMatrix.get("East Africa"), 1);
		CountryMatrix.get("Central Africa").setConnection(CountryMatrix.get("East Africa"), 1);
		CountryMatrix.get("Central Africa").setConnection(CountryMatrix.get("South Africa"), 1);
		CountryMatrix.get("East Africa").setConnection(CountryMatrix.get("Middle East"), 1);
		CountryMatrix.get("East Africa").setConnection(CountryMatrix.get("South Africa"), 1);
		CountryMatrix.get("East Africa").setConnection(CountryMatrix.get("Madagascar"), 1);
		CountryMatrix.get("South Africa").setConnection(CountryMatrix.get("Madagascar"), 1);
		CountryMatrix.get("West Australia").setConnection(CountryMatrix.get("East Australia"), 1);
		CountryMatrix.get("West Australia").setConnection(CountryMatrix.get("Indonesia"), 1);
		CountryMatrix.get("East Australia").setConnection(CountryMatrix.get("New Guinea"), 1);
		CountryMatrix.get("Indonesia").setConnection(CountryMatrix.get("New Guinea"), 1);
		CountryMatrix.get("Indonesia").setConnection(CountryMatrix.get("Korea"), 1);
		CountryMatrix.get("West Russia").setConnection(CountryMatrix.get("Afghanistan"), 1);
		CountryMatrix.get("West Russia").setConnection(CountryMatrix.get("China"), 1);
		CountryMatrix.get("West Russia").setConnection(CountryMatrix.get("CentralWest Russia"), 1);
		CountryMatrix.get("CentralWest Russia").setConnection(CountryMatrix.get("China"), 1);
		CountryMatrix.get("CentralWest Russia").setConnection(CountryMatrix.get("Mongolia"), 1);
		CountryMatrix.get("CentralWest Russia").setConnection(CountryMatrix.get("South Russia"), 1);
		CountryMatrix.get("CentralWest Russia").setConnection(CountryMatrix.get("CentralEast Russia"), 1);
		CountryMatrix.get("CentralEast Russia").setConnection(CountryMatrix.get("East Russia"), 1);
		CountryMatrix.get("CentralEast Russia").setConnection(CountryMatrix.get("South Russia"), 1);
		CountryMatrix.get("East Russia").setConnection(CountryMatrix.get("South Russia"), 1);
		CountryMatrix.get("East Russia").setConnection(CountryMatrix.get("Mongolia"), 1);
		CountryMatrix.get("East Russia").setConnection(CountryMatrix.get("Japan"), 1);
		CountryMatrix.get("Middle East").setConnection(CountryMatrix.get("Afghanistan"), 1);
		CountryMatrix.get("Middle East").setConnection(CountryMatrix.get("India"), 1);
		CountryMatrix.get("Afghanistan").setConnection(CountryMatrix.get("China"), 1);
		CountryMatrix.get("Afghanistan").setConnection(CountryMatrix.get("India"), 1);
		CountryMatrix.get("India").setConnection(CountryMatrix.get("China"), 1);
		CountryMatrix.get("India").setConnection(CountryMatrix.get("Korea"), 1);
		CountryMatrix.get("South Russia").setConnection(CountryMatrix.get("Mongolia"), 1);
		CountryMatrix.get("Mongolia").setConnection(CountryMatrix.get("China"), 1);
		CountryMatrix.get("Mongolia").setConnection(CountryMatrix.get("Japan"), 1);
		CountryMatrix.get("China").setConnection(CountryMatrix.get("Korea"), 1);
		
	}
	
	private Country StringToCountry(String s){
		return CountryMatrix.get(s);
	}
	
	private Color getCountryColor(String country){
		switch(StringToCountry(country).getOwner()){
			case "RED": return RED;
			case "BLUE": return BLUE;
			case "GREEN": return GREEN;
			case "YELLOW": return YELLOW;
			case "PINK": return PINK;
			case "TEAL": return TEAL;
		}
		return null;
	}
	
	private boolean isNeighbor(Country C1, Country C2){
		for (int i = 0;i<C1.getConnections().size();i++){
			if(C2 == C1.getConnections().get(i))
			return true;
		}
		return false;
	}
	/**
	 * takes a country and a player and sets that countries owner to the player
	 * currently the player is represented as a string
	 * @param country the selected country
	 * @param Player the player to own this country
	 */
	public void setOwnerOfCountry(String country, String Player){
		StringToCountry(country).setOwner(Player);
	}
	/**
	 * takes a country and returns that countries owner
	 * @param country to be checked
	 * @return a Player as a String
	 */
	public String getOwnerOfCountry(String country){
		return StringToCountry(country).getOwner();
	}
	
	/**
	 * When a country is clicked this method checks if it's a neighbor of the last clicked country and
	 * highlights the clicked country and the past one if this one was it's neighbor
	 * @param s string of clicked country
	 */
	public void clickedCountry(String s){
		if(!(s.equals("-1"))){
			if(current != null){
				if(current != StringToCountry(s)){
					if(isNeighbor(current, StringToCountry(s))){
						if(pre_current != null){
							//gr.getGame().drawCountry(gr.getMain_frame().getGraphics(), pre_current.toString(), getCountryColor(pre_current.toString()));
						}
						//gr.getGame().drawCountry(gr.getMain_frame().getGraphics(), current.toString(), SELECTED);
						//gr.getGame().drawCountry(gr.getMain_frame().getGraphics(), s, SELECTED);
						if(current.getOwner().equals(StringToCountry(s).getOwner())){
							// ----------- SAME OWNER OF BOTH COUNTRIES DO SOMETHING HERE --------------
							System.out.println("SAME");
						}
						else{
							// ----------- DIFFRENT OWNER OF COUNTRIES DO SOMETHING HERE --------------
							System.out.println("DIFFRENT");
						}
					}
					else{
						if(pre_current != null){
							//gr.getGame().drawCountry(gr.getMain_frame().getGraphics(), pre_current.toString(), getCountryColor(pre_current.toString()));
						}
						//gr.getGame().drawCountry(gr.getMain_frame().getGraphics(), current.toString(), getCountryColor(current.toString()));
						//gr.getGame().drawCountry(gr.getMain_frame().getGraphics(), s, SELECTED);
					}
				}
				pre_current = current;
				current = StringToCountry(s);
			}
			else{
				pre_current = current;
				current = StringToCountry(s);
				//gr.getGame().drawCountry(gr.getMain_frame().getGraphics(), s, SELECTED);
			}
		}
		else{
			//gr.getGame().paint(gr.getMain_frame().getGraphics());
			current = null;
			pre_current = null;
		}
	}
}
