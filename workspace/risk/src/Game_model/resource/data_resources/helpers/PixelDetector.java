package Game_model.resource.data_resources.helpers;

import java.awt.Color;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.imageio.ImageIO;

public class PixelDetector {

	private File img;
	private Color[] pixelColors = new Color[43];
	private BufferedImage buffImg = new BufferedImage(1280, 765, BufferedImage.TYPE_INT_RGB);
	private HashMap<String, ArrayList<Point>> Map;

	public PixelDetector(String s){
		img = new File(s);
		setColors();
		try { buffImg = ImageIO.read(img); } catch (IOException e) { }
		Map = getFromColorArray();
	}
	
	public HashMap<String, ArrayList<Point>> getMap(){
		return Map;
	}
	
	private HashMap<String, ArrayList<Point>> getFromColorArray(){
		HashMap<String, ArrayList<Point>> CountryPixelMatrix = new HashMap<String, ArrayList<Point>>();
		ArrayList<Point> Alaska = new ArrayList<Point>();
		ArrayList<Point> North_Canada = new ArrayList<Point>();
		ArrayList<Point> West_Canada = new ArrayList<Point>();
		ArrayList<Point> Central_Canada = new ArrayList<Point>();
		ArrayList<Point> East_Canada = new ArrayList<Point>();
		ArrayList<Point> West_USA = new ArrayList<Point>();
		ArrayList<Point> East_USA = new ArrayList<Point>();
		ArrayList<Point> Mexico = new ArrayList<Point>();
		ArrayList<Point> Greenland = new ArrayList<Point>();
		ArrayList<Point> North_South_America = new ArrayList<Point>();
		ArrayList<Point> West_South_America = new ArrayList<Point>();
		ArrayList<Point> East_South_America = new ArrayList<Point>();
		ArrayList<Point> South_South_America = new ArrayList<Point>();
		ArrayList<Point> Iceland = new ArrayList<Point>();
		ArrayList<Point> Scandinavia = new ArrayList<Point>();
		ArrayList<Point> Great_Britain = new ArrayList<Point>();
		ArrayList<Point> Central_Europe = new ArrayList<Point>();
		ArrayList<Point> West_Europe = new ArrayList<Point>();
		ArrayList<Point> South_Europe = new ArrayList<Point>();
		ArrayList<Point> East_Europe = new ArrayList<Point>();
		ArrayList<Point> Northwest_Africa = new ArrayList<Point>();
		ArrayList<Point> North_Africa = new ArrayList<Point>();
		ArrayList<Point> Central_Africa = new ArrayList<Point>();
		ArrayList<Point> East_Africa = new ArrayList<Point>();
		ArrayList<Point> South_Africa = new ArrayList<Point>();
		ArrayList<Point> Madagascar = new ArrayList<Point>();
		ArrayList<Point> West_Australia = new ArrayList<Point>();
		ArrayList<Point> East_Australia = new ArrayList<Point>();
		ArrayList<Point> Indonesia = new ArrayList<Point>();
		ArrayList<Point> New_Guinea = new ArrayList<Point>();
		ArrayList<Point> West_Russia = new ArrayList<Point>();
		ArrayList<Point> CentralWest_Russia = new ArrayList<Point>();
		ArrayList<Point> CentralEast_Russia = new ArrayList<Point>();
		ArrayList<Point> East_Russia = new ArrayList<Point>();
		ArrayList<Point> Middle_East = new ArrayList<Point>();
		ArrayList<Point> Afghanistan = new ArrayList<Point>();
		ArrayList<Point> India = new ArrayList<Point>();
		ArrayList<Point> South_Russia = new ArrayList<Point>();
		ArrayList<Point> Mongolia = new ArrayList<Point>();
		ArrayList<Point> China = new ArrayList<Point>();
		ArrayList<Point> Korea = new ArrayList<Point>();
		ArrayList<Point> Japan = new ArrayList<Point>();		
		ArrayList<Point> Border = new ArrayList<Point>();
		
		for(int i = 0;i<buffImg.getWidth();i++){
			for(int j = 0;j<buffImg.getHeight();j++){	
				if(pixelColors[0].getRGB() == buffImg.getRGB(i,j)){
					Alaska.add(new Point(i,j));
				}
				else if (pixelColors[1].getRGB() == buffImg.getRGB(i,j)){
					North_Canada.add(new Point(i,j));
				}
				else if (pixelColors[2].getRGB() == buffImg.getRGB(i,j)){
					West_Canada.add(new Point(i,j));
				}
				else if (pixelColors[3].getRGB() == buffImg.getRGB(i,j)){
					Central_Canada.add(new Point(i,j));
				}
				else if (pixelColors[4].getRGB() == buffImg.getRGB(i,j)){
					East_Canada.add(new Point(i,j));
				}
				else if (pixelColors[5].getRGB() == buffImg.getRGB(i,j)){
					West_USA.add(new Point(i,j));
				}
				else if (pixelColors[6].getRGB() == buffImg.getRGB(i,j)){
					East_USA.add(new Point(i,j));
				}
				else if (pixelColors[7].getRGB() == buffImg.getRGB(i,j)){
					Mexico.add(new Point(i,j));
				}
				else if (pixelColors[8].getRGB() == buffImg.getRGB(i,j)){
					Greenland.add(new Point(i,j));
				}
				else if (pixelColors[9].getRGB() == buffImg.getRGB(i,j)){
					North_South_America.add(new Point(i,j));
				}
				else if (pixelColors[10].getRGB() == buffImg.getRGB(i,j)){
					West_South_America.add(new Point(i,j));
				}
				else if (pixelColors[11].getRGB() == buffImg.getRGB(i,j)){
					East_South_America.add(new Point(i,j));
				}
				else if (pixelColors[12].getRGB() == buffImg.getRGB(i,j)){
					South_South_America.add(new Point(i,j));
				}
				else if (pixelColors[13].getRGB() == buffImg.getRGB(i,j)){
					Iceland.add(new Point(i,j));
				}
				else if (pixelColors[14].getRGB() == buffImg.getRGB(i,j)){
					Scandinavia.add(new Point(i,j));
				}
				else if (pixelColors[15].getRGB() == buffImg.getRGB(i,j)){
					Great_Britain.add(new Point(i,j));
				}
				else if (pixelColors[16].getRGB() == buffImg.getRGB(i,j)){
					Central_Europe.add(new Point(i,j));
				}
				else if (pixelColors[17].getRGB() == buffImg.getRGB(i,j)){
					West_Europe.add(new Point(i,j));
				}
				else if (pixelColors[18].getRGB() == buffImg.getRGB(i,j)){
					South_Europe.add(new Point(i,j));
				}
				else if (pixelColors[19].getRGB() == buffImg.getRGB(i,j)){
					East_Europe.add(new Point(i,j));
				}
				else if (pixelColors[20].getRGB() == buffImg.getRGB(i,j)){
					Northwest_Africa.add(new Point(i,j));
				}
				else if (pixelColors[21].getRGB() == buffImg.getRGB(i,j)){
					North_Africa.add(new Point(i,j));
				}
				else if (pixelColors[22].getRGB() == buffImg.getRGB(i,j)){
					Central_Africa.add(new Point(i,j));
				}
				else if (pixelColors[23].getRGB() == buffImg.getRGB(i,j)){
					East_Africa.add(new Point(i,j));
				}
				else if (pixelColors[24].getRGB() == buffImg.getRGB(i,j)){					
					South_Africa.add(new Point(i,j));
				}
				else if (pixelColors[25].getRGB() == buffImg.getRGB(i,j)){					
					Madagascar.add(new Point(i,j));
				}
				else if (pixelColors[26].getRGB() == buffImg.getRGB(i,j)){					
					West_Australia.add(new Point(i,j));
				}
				else if (pixelColors[27].getRGB() == buffImg.getRGB(i,j)){					
					East_Australia.add(new Point(i,j));
				}
				else if (pixelColors[28].getRGB() == buffImg.getRGB(i,j)){					
					Indonesia.add(new Point(i,j));
				}
				else if (pixelColors[29].getRGB() == buffImg.getRGB(i,j)){					
					New_Guinea.add(new Point(i,j));
				}
				else if (pixelColors[30].getRGB() == buffImg.getRGB(i,j)){
					West_Russia.add(new Point(i,j));
				}
				else if (pixelColors[31].getRGB() == buffImg.getRGB(i,j)){
					CentralWest_Russia.add(new Point(i,j));
				}
				else if (pixelColors[32].getRGB() == buffImg.getRGB(i,j)){
					CentralEast_Russia.add(new Point(i,j));
				}
				else if (pixelColors[33].getRGB() == buffImg.getRGB(i,j)){
					East_Russia.add(new Point(i,j));
				}
				else if (pixelColors[34].getRGB() == buffImg.getRGB(i,j)){
					Middle_East.add(new Point(i,j));
				}
				else if (pixelColors[35].getRGB() == buffImg.getRGB(i,j)){
					Afghanistan.add(new Point(i,j));
				}
				else if (pixelColors[36].getRGB() == buffImg.getRGB(i,j)){
					India.add(new Point(i,j));
				}
				else if (pixelColors[37].getRGB() == buffImg.getRGB(i,j)){
					South_Russia.add(new Point(i,j));
				}
				else if (pixelColors[38].getRGB() == buffImg.getRGB(i,j)){
					Mongolia.add(new Point(i,j));
				}
				else if (pixelColors[39].getRGB() == buffImg.getRGB(i,j)){
					China.add(new Point(i,j));
				}
				else if (pixelColors[40].getRGB() == buffImg.getRGB(i,j)){
					Korea.add(new Point(i,j));
				}
				else if (pixelColors[41].getRGB() == buffImg.getRGB(i,j)){
					Japan.add(new Point(i,j));
				}
				else if (pixelColors[42].getRGB() == buffImg.getRGB(i,j)){
					Border.add(new Point(i,j));
				}
			}
		}
		CountryPixelMatrix.put("Alaska", Alaska);
		CountryPixelMatrix.put("North Canada", North_Canada);
		CountryPixelMatrix.put("West Canada", West_Canada);
		CountryPixelMatrix.put("Central Canada", Central_Canada);
		CountryPixelMatrix.put("East Canada", East_Canada);
		CountryPixelMatrix.put("West USA", West_USA);
		CountryPixelMatrix.put("East USA", East_USA);
		CountryPixelMatrix.put("Mexico", Mexico);
		CountryPixelMatrix.put("Greenland", Greenland);
		CountryPixelMatrix.put("North South America", North_South_America);
		CountryPixelMatrix.put("West South America", West_South_America);
		CountryPixelMatrix.put("East South America", East_South_America);
		CountryPixelMatrix.put("South South America", South_South_America);
		CountryPixelMatrix.put("Iceland", Iceland);
		CountryPixelMatrix.put("Scandinavia", Scandinavia);
		CountryPixelMatrix.put("Great Britain", Great_Britain);
		CountryPixelMatrix.put("Central Europe", Central_Europe);
		CountryPixelMatrix.put("West Europe", West_Europe);
		CountryPixelMatrix.put("South Europe", South_Europe);
		CountryPixelMatrix.put("East Europe", East_Europe);
		CountryPixelMatrix.put("Northwest Africa", Northwest_Africa);
		CountryPixelMatrix.put("North Africa", North_Africa);
		CountryPixelMatrix.put("Central Africa", Central_Africa);
		CountryPixelMatrix.put("East Africa", East_Africa);
		CountryPixelMatrix.put("South Africa", South_Africa);
		CountryPixelMatrix.put("Madagascar", Madagascar);
		CountryPixelMatrix.put("West Australia", West_Australia);
		CountryPixelMatrix.put("East Australia", East_Australia);
		CountryPixelMatrix.put("Indonesia", Indonesia);
		CountryPixelMatrix.put("New Guinea", New_Guinea);
		CountryPixelMatrix.put("West Russia", West_Russia);
		CountryPixelMatrix.put("CentralWest Russia", CentralWest_Russia);
		CountryPixelMatrix.put("CentralEast Russia", CentralEast_Russia);
		CountryPixelMatrix.put("East Russia", East_Russia);
		CountryPixelMatrix.put("Middle East", Middle_East);
		CountryPixelMatrix.put("Afghanistan", Afghanistan);
		CountryPixelMatrix.put("India", India);
		CountryPixelMatrix.put("South Russia", South_Russia);
		CountryPixelMatrix.put("Mongolia", Mongolia);
		CountryPixelMatrix.put("China", China);
		CountryPixelMatrix.put("Korea", Korea);
		CountryPixelMatrix.put("Japan", Japan);
		
		return CountryPixelMatrix;
	}
	
	
	private void setColors(){
		Color Alaska = new Color(76,76,76);
		Color North_Canada = new Color(79,79,79);
		Color West_Canada = new Color(82,82,82);
		Color Central_Canada = new Color(84,84,84);
		Color East_Canada = new Color(87,87,87);
		Color West_USA = new Color(89,89,89);
		Color East_USA = new Color(92,92,92);
		Color Mexico = new Color(94,94,94);
		Color Greenland = new Color(97,97,97);
		Color North_South_America = new Color(99,99,99);
		Color West_South_America = new Color(102,102,102);
		Color East_South_America = new Color(105,105,105);
		Color South_South_America = new Color(107,107,107);
		Color Iceland = new Color(110,110,110);
		Color Scandinavia = new Color(112,112,112);
		Color Great_Britain = new Color(115,115,115);
		Color Central_Europe = new Color(117,117,117);
		Color West_Europe = new Color(120,120,120);
		Color South_Europe = new Color(122,122,122);
		Color East_Europe = new Color(125,125,125);
		Color Northwest_Africa = new Color(128,128,128);
		Color North_Africa = new Color(130,130,130);
		Color Central_Africa = new Color(133,133,133);
		Color East_Africa = new Color(135,135,135);
		Color South_Africa = new Color(138,138,138);
		Color Madagascar = new Color(140,140,140);
		Color West_Australia = new Color(143,143,143);
		Color East_Australia = new Color(145,145,145);
		Color Indonesia = new Color(148,148,148);
		Color New_Guinea = new Color(150,150,150);
		Color West_Russia = new Color(153,153,153);
		Color CentralWest_Russia = new Color(156,156,156);
		Color CentralEast_Russia = new Color(158,158,158);
		Color East_Russia = new Color(161,161,161);
		Color Middle_East = new Color(163,163,163);
		Color Afghanistan = new Color(166,166,166);
		Color India = new Color(168,168,168);
		Color South_Russia = new Color(171,171,171);
		Color Mongolia = new Color(173,173,173);
		Color China = new Color(176,176,176);
		Color Korea = new Color(178,178,178);
		Color Japan = new Color(181,181,181);
		Color Border = new Color(0,0,0);
		pixelColors[0] = Alaska;
		pixelColors[1] = North_Canada;
		pixelColors[2] = West_Canada;
		pixelColors[3] = Central_Canada;
		pixelColors[4] = East_Canada;
		pixelColors[5] = West_USA;
		pixelColors[6] = East_USA;
		pixelColors[7] = Mexico;
		pixelColors[8] = Greenland;
		pixelColors[9] = North_South_America;
		pixelColors[10] = West_South_America;
		pixelColors[11] = East_South_America;
		pixelColors[12] = South_South_America;
		pixelColors[13] = Iceland;
		pixelColors[14] = Scandinavia;
		pixelColors[15] = Great_Britain;
		pixelColors[16] = Central_Europe;
		pixelColors[17] = West_Europe;
		pixelColors[18] = South_Europe;
		pixelColors[19] = East_Europe;
		pixelColors[20] = Northwest_Africa;
		pixelColors[21] = North_Africa;
		pixelColors[22] = Central_Africa;
		pixelColors[23] = East_Africa;
		pixelColors[24] = South_Africa;
		pixelColors[25] = Madagascar;
		pixelColors[26] = West_Australia;
		pixelColors[27] = East_Australia;
		pixelColors[28] = Indonesia;
		pixelColors[29] = New_Guinea;
		pixelColors[30] = West_Russia;
		pixelColors[31] = CentralWest_Russia ;
		pixelColors[32] = CentralEast_Russia;
		pixelColors[33] = East_Russia;
		pixelColors[34] = Middle_East;
		pixelColors[35] = Afghanistan;
		pixelColors[36] = India;
		pixelColors[37] = South_Russia;
		pixelColors[38] = Mongolia;
		pixelColors[39] = China;
		pixelColors[40] = Korea;
		pixelColors[41] = Japan;
		pixelColors[42] = Border;
	}
}
