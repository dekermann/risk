package Game_model.resource;

import Game_controller.Game_handler;
import Game_model.resource.data_resources.Pixel_data_resources;
import Game_model.resource.image_resources.Game_Image_resources;
import Game_model.state.State_manager;
import Game_view.Game;
import Game_view.View;

public class Game_resources extends Thread {

	private Game_Image_resources gir;
	private Pixel_data_resources pdr;
	private Game_model.state.State gs;
	private State_manager sm;
	private View main_frame;

	private boolean loading_done = false;
	private long start_time;
	private double time_load = 5000;
	
	/**
	 * Connecting the controller and view
	 */
	private Game_handler gh;
	private Game game;
	
	public Game_resources(State_manager sm) {
		this.sm = sm;
	}
	
	public void setGame(Game game){
		this.game = game;
	}
	
	public void setGame_state(Game_model.state.State game_state){
		this.gs = game_state;
	}
	
	public void setMain_frame(View main_frame) {
		this.main_frame = main_frame;
	}
	
	public void setGame_handler(Game_handler gh){
		this.gh = gh;
	}
	
	public Game_handler getGame_handler(){
		return gh;
	}
	
	public View getMain_frame() {
		return main_frame;
	}
	
	public State_manager getState_manager(){
		return sm;
	}
	
	public Game getGame(){
		return game;
	}
	
	public Game_model.state.State getGame_state(){
		return gs;
	}
	
	public Game_Image_resources getGameImages() {
		return this.gir;
	}

	public Pixel_data_resources getPdr() {
		return pdr;
	}

	/** Methods for checking if the game_rw are done loading
	 * This class i started in a separate thread
	 */
	public synchronized boolean done_loading() {
		return this.loading_done;
	}
	
	public synchronized double get_percent_loading() {
		return (System.currentTimeMillis() - start_time)/time_load;
	}

	/**
	 * Run method that will start when the thread is started
	 * in state_manager
	 */
	public void run() {
		start_time = System.currentTimeMillis();
		this.gir = new Game_Image_resources();
		this.pdr = new Pixel_data_resources();
		loading_done = true;
	}

}
