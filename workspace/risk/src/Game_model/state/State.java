package Game_model.state;

import java.awt.Graphics;

import javax.swing.JPanel;

import Game_model.resource.Game_resources;

public interface State {

	public void update();
	public void draw(Graphics g);
	public void enter();
	public void exit();
	public String getState();
	public String changeState();
	public void loadResources(Game_resources gr);
	public JPanel getComponent();
}
