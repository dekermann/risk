package Util;

public class Stopwatch {
	private long start, stop, fps;
	
	public Stopwatch(int fps) {
		setFps(fps);
		reset();
	}

	public void start() {
		reset();
		setStart(System.currentTimeMillis());
	}
	
	public void stop() {
		setStop(System.currentTimeMillis());
	}
	
	public long waitTime() {
		return getStop()-getStart() < getFps() ? getFps()-(getStop()-getStart()) : 0;
	}
	
	private void reset() {
		setStop(0);
		setStart(0);
	}

//Getter and setter methods
	private long getStart() {
		return start;
	}

	private void setStart(long start) {
		this.start = start;
	}

	private long getStop() {
		return stop;
	}

	private void setStop(long stop) {
		this.stop = stop;
	}

	private long getFps() {
		return fps;
	}

	public void setFps(long fps) {
		this.fps = 1000/fps;
	}
		
}
