

import java.awt.AWTException;

import Util.Stopwatch;

public class Risk {
	
	@SuppressWarnings("static-access")
	public static void main(String[] args) throws InterruptedException, AWTException {
		Stopwatch sw = new Stopwatch(25);
		while (true) {
			sw.start();
			sw.stop();
			Thread.currentThread().sleep(sw.waitTime());
		}
	}
}
