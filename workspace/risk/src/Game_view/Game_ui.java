package Game_view;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.ImageIcon;

public class Game_ui {
	
	private ImageIcon border_img = new ImageIcon("border_ui.png");
	private final int border_img_w = 100;
	private final int border_img_h = 25;
	
	private int ui_width, ui_height;

	public Game_ui(int game_width, int game_height) {
		init(game_width, game_height);
	}
	public void draw_ui(Graphics g) {
		g.setColor(Color.white);
		g.fillRect(0, ui_height, ui_width, 800);
		g.setColor(Color.black);
		g.fillRect(0, ui_height, ui_width, 25);
	}
	
	private void init(int game_width, int game_height) {
		if (game_width == 0 && game_height == 0) {
			try {
				throw new Exception("Varning in width och height i game_ui �r 0 - se till att den f�r " +
						" \n med sig r�tt kordinater fr�n Game-panelen");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		this.ui_width = game_width;
		this.ui_height = (int) (game_height * 0.8);
	}
}
